//
//  AppDelegate.h
//  quiz
//
//  Created by Tin on 14/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, NSURLConnectionDelegate, NSURLConnectionDataDelegate> {
    NSMutableData *data;
}

@property NSMutableArray *vragen;
@property (unsafe_unretained) Class mode;
 
@property (strong, nonatomic) UIWindow *window;

@end
