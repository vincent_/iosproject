//
//  ModeDelegate.h
//  quiz
//
//  Created by Jonas on 20/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GameController;

@protocol ModeDelegate <NSObject>
 
@required
- (NSString *)getTitle: (GameController *)game;
- (bool)hasRemaining: (GameController *)game;
- (void)onCorrectAnswer: (GameController *)game;
- (void)onNextRound: (GameController *)game;
- (void)onTick: (GameController *)game;
- (void)onTimeout: (GameController *)game;
- (void)onSwipe:(GameController*)game;

@end