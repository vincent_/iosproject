//
//  TimeAttackMode.m
//  quiz
//
//  Created by Jonas on 20/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import "TimeAttackMode.h"
#import "GameController.h"

@implementation TimeAttackMode

-(void)onCorrectAnswer:(GameController *)game {
    game.time += 40;
    if(game.time > 200) {
        game.time = 200;
    }
    game.score++;
    game.scoreLabel.text = [NSString stringWithFormat:@"Score: %d", game.score];
}

-(void)onTick:(GameController *)game {
    if(game.time > 0) {
        game.vraagCountLabel.text = [NSString stringWithFormat:@"Tijd: %ds",game.time/4];
    }
    [game.progress setProgress:game.time/200.0];
}
 
-(void)onSwipe:(GameController *)game {
    [game next];
}

-(NSString *)getTitle:(GameController *)game {
    return @"Tijdsaanval";
}
-(bool)hasRemaining:(GameController *)game {
    return game.time > 0;
}

- (void)onTimeout:(GameController *)game { }
- (void)onNextRound:(GameController *)game { }

@end
