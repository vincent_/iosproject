//
//  OptionsViewController.h
//  quiz
//
//  Created by Tin on 14/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OptionsViewController : UIViewController {
    IBOutlet UILabel *versieLabel;
    IBOutlet UILabel *vragenLabel;
}

@property UILabel *versieLabel;
@property UILabel *vragenLabel;

-(IBAction)resetSettings:(id)sender;

@end
 