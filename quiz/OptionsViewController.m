//
//  OptionsViewController.m
//  quiz
//
//  Created by Tin on 14/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import "OptionsViewController.h"
#import "DatabaseManager.h"

@interface OptionsViewController ()

@end

@implementation OptionsViewController

@synthesize versieLabel;
@synthesize vragenLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Opties";
    DatabaseManager *db = [DatabaseManager instance];
    vragenLabel.text = [NSString stringWithFormat:@"%d", [db getTotalVragenCount]];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSInteger versie = [prefs integerForKey:@"version"];
    versieLabel.text = [NSString stringWithFormat:@"%d", versie];
	// Do any additional setup after loading the view.
}
 
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)resetSettings:(id)sender {
    DatabaseManager * db = [DatabaseManager instance];
    [db reset];
}

@end
