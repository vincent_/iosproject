//
//  DatabaseManager.m
//  quiz
//
//  Created by Jonas on 20/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import "DatabaseManager.h"
#import "Vraag.h"

@implementation DatabaseManager

@synthesize database;

-(NSInteger) getSolvedCount:(int) categorie {
    NSString *querySQL = [NSString stringWithFormat: @"SELECT count(*) FROM vragen WHERE categorie=\"%d\" AND done = 1", categorie];
    NSInteger count;
    const char *query_stmt = [querySQL UTF8String];
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
    {
        if (sqlite3_step(statement) == SQLITE_ROW)
        {
             count = [[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)] intValue];
            
        } else {
            return 0;
        }
        sqlite3_finalize(statement);
    }
    return count;
}

-(NSInteger) getTotalCount:(int) categorie {
    NSString *querySQL = [NSString stringWithFormat: @"SELECT count(*) FROM vragen WHERE categorie=\"%d\"", categorie];
    NSInteger count;
    const char *query_stmt = [querySQL UTF8String];
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
    {
        if (sqlite3_step(statement) == SQLITE_ROW)
        {
            count = [[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)] intValue];
            
        } else {
            count = 0;
        }
        sqlite3_finalize(statement);
    }
    return count;
}

-(NSInteger) getTotalVragenCount {
    NSInteger count;
    const char *query_stmt = "SELECT count(*) FROM vragen";
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
    {
        if (sqlite3_step(statement) == SQLITE_ROW)
        {
            count = [[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)] intValue];
            
        } else { 
            count = 0;
        }
        sqlite3_finalize(statement);
    }
    return count;
}

-(NSMutableArray *)getVragen:(NSArray *) categorieen {
    NSMutableArray *vragen = [[NSMutableArray alloc] init];
    NSMutableArray *cat = [[NSMutableArray alloc] init];
    for(NSIndexPath *path in categorieen) {
        [cat addObject:[NSNumber numberWithInt:[path row] +1]];
    }
    NSString *joinedString = [cat componentsJoinedByString:@","];

    
    NSString *querySQL = [NSString stringWithFormat: @"SELECT * FROM vragen WHERE categorie IN(%@) AND done = 0", joinedString];
    NSLog(@"Query: %@", querySQL);
    const char *query_stmt = [querySQL UTF8String];
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            Vraag *vraag = [[Vraag alloc] init];
            vraag.uid = [[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)] intValue];
            vraag.vraag = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
            vraag.antwoord = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)];
            vraag.categorie = [[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)] intValue];
            [vragen addObject:vraag];
        }
        sqlite3_finalize(statement);
    }
    NSLog(@"Aantal vragen: %d", [vragen count]);
    for (uint i = 0; i < vragen.count; ++i)
    {
        // Select a random element between i and end of array to swap with.
        int nElements = vragen.count - i;
        int n = arc4random_uniform(nElements) + i;
        [vragen exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
    return vragen;
}

-(void)updateVraag:(Vraag *)vraag {
    NSString *querySQL = [NSString stringWithFormat: @"INSERT OR REPLACE INTO vragen(id, vraag, antwoord, categorie, done) VALUES(%d, '%@', '%@', %d, 0)", vraag.uid, vraag.vraag, vraag.antwoord, vraag.categorie];
    const char *query_stmt = [querySQL UTF8String];
    char *error;
    sqlite3_exec(database, query_stmt, NULL, NULL, &error);
    if(error) {
        NSLog(@"db update error: %s", error);
    }
}

-(void)reset {
    NSString *querySQL = [NSString stringWithFormat: @"UPDATE vragen SET done = 0"];
    const char *query_stmt = [querySQL UTF8String];
    char *error;
    sqlite3_exec(database, query_stmt, NULL, NULL, &error);
    if(error) {
        NSLog(@"db resetdone error: %s", error);
    }
}
  
-(void)setDone:(Vraag *)vraag {
    NSString *querySQL = [NSString stringWithFormat: @"UPDATE vragen SET done = 1 WHERE id = %d", vraag.uid];
    const char *query_stmt = [querySQL UTF8String];
    char *error;
    sqlite3_exec(database, query_stmt, NULL, NULL, &error);
    if(error) {
        NSLog(@"db setdone error: %s", error);
    }
}

static DatabaseManager *instance;

-(id)init {
    NSLog(@"-init");
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
     
    if(sqlite3_open([[[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:@"quiz.sqlite"]] UTF8String], &database) == SQLITE_OK) {
        NSLog(@"DB OK");
    }
    return self;
}

+ (void) initialize {
    instance = [[DatabaseManager alloc] init];
    NSLog(@"+init");
}

+ (id) instance {
    return instance;
}

@end
