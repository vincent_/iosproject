//
//  TopscoreController.m
//  quiz
//
//  Created by Jonas on 20/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import "TopscoreController.h"
#import "Score.h"

@interface TopscoreController ()

@end

@implementation TopscoreController

@synthesize tableView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Normaal";
    
    UISwipeGestureRecognizer *mSwipeRightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipeRight:)];
    
    [mSwipeRightRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    
    [self.view addGestureRecognizer:mSwipeRightRecognizer];
    UISwipeGestureRecognizer *mSwipeLeftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipeLeft:)];
    
    [mSwipeLeftRecognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    
    [self.view addGestureRecognizer:mSwipeLeftRecognizer];
    [self reloadData];

}

- (void) reloadData {
    reloading = true;
    // Do any additional setup after loading the view.
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://spacedicks.herokuapp.com/highscores/?mode=%@",self.title]];
    data = [[NSMutableData alloc] init];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (!connection) {
        // this is better if you @throw an exception here
        NSLog(@"error while starting the connection");
    }
}

- (void) onSwipeLeft:(UISwipeGestureRecognizer*)swipe {
    if(reloading) {
        return;
    }
    if(self.title == @"Normaal") {
        self.title = @"Overleving";
    } else if(self.title == @"Overleving") {
        self.title = @"Tijdsaanval";
    } else if(self.title == @"Tijdsaanval") {
        self.title = @"Normaal";
    }
    [self reloadData];
}

- (void) onSwipeRight:(UISwipeGestureRecognizer*)swipe {
    if(reloading) {
        return;
    }
    if(self.title == @"Overleving") {
        self.title = @"Normaal";
    } else if(self.title == @"Tijdsaanval") {
        self.title = @"Overleving";
    } else if(self.title == @"Normaal") {
        self.title = @"Tijdsaanval";
    }
    [self reloadData];
}



- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [data setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)someData {
    [data appendData:someData];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Error %@", error);
    reloading = false;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSError *error;
    id jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    data = nil;
    if(jsonData) {
        NSLog(@"JSON: %@", jsonData);
        topscores = [[NSMutableArray alloc] init];
        //NSArray* jsonItems = [jsonData objectForKey:@"items"];
        for(NSDictionary *topscore in jsonData) {
            Score *score = [[Score alloc] init];
            score.naam = [topscore objectForKey:@"naam"];
            score.score = [[topscore objectForKey:@"score"] intValue];
            [topscores addObject:score];
        }
        [tableView reloadData];
    } else {
        NSLog(@"Error %@", error);
    }
    reloading = false;

}
 
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [topscores count];
}

- (UITableViewCell *)tableView:(UITableView *)tv
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell2";
    
    UITableViewCell *cell = [tv
                             dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle
                reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell.
    UIImage *cellImage = [UIImage imageNamed:@"apple.png"];
    cell.imageView.image = cellImage;
    Score *score = [topscores
                             objectAtIndex: [indexPath row]];
    
    cell.textLabel.text = score.naam;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Score: %d", score.score];
    return cell;
}
@end
