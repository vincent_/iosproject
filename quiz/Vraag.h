//
//  Vraag.h
//  quiz
//
//  Created by Jonas on 20/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface Vraag : NSObject {
    sqlite3 *database;
}

@property NSString *vraag;
@property NSString *antwoord;
@property NSInteger categorie;
@property NSInteger uid;
  
@end
  