//
//  ChooseModeController.h
//  quiz
//
//  Created by Jonas on 20/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseModeController : UIViewController

-(IBAction)selectNormal:(id)sender;
-(IBAction)selectSurvival:(id)sender;
-(IBAction)selectTimeAttack:(id)sender;

@end
 