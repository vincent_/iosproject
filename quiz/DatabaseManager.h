//
//  DatabaseManager.h
//  quiz
//
//  Created by Jonas on 20/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@class Vraag;

@interface DatabaseManager : NSObject

@property sqlite3 *database;

-(NSInteger) getSolvedCount:(int) categorie;
-(NSInteger) getTotalCount:(int) categorie;
-(NSMutableArray *)getVragen:(NSArray *) categorieen;
-(NSInteger) getTotalVragenCount;
-(void)updateVraag:(Vraag *)vraag;
-(void)reset;
-(void)setDone:(Vraag *)vraag;

+(id)instance; 

@end

 