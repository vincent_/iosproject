//
//  AppDelegate.m
//  quiz
//
//  Created by Tin on 14/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import "AppDelegate.h"
#import "Vraag.h"
#import "DatabaseManager.h"

@implementation AppDelegate

@synthesize vragen;
@synthesize mode;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [self copyDB];
    [self updateDB];
    return YES;
}

- (void)copyDB {
    // First, test for existence.
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"quiz.sqlite"];
    success = [fileManager fileExistsAtPath:writableDBPath];
    if (success) {
        NSLog(@"Database al gekopieerd");
        return;
    }
    // The writable database does not exist, so copy the default to the appropriate location.
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"quiz.sqlite"];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    if (!success) {
        NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
}
 
- (void) updateDB {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSInteger versie = [prefs integerForKey:@"version"];
    if(versie == 0) {
        versie = 1;
    }
    NSLog(@"Updaten vanaf versie %d", versie);
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://spacedicks.herokuapp.com/questions/?versie=%d",versie]];
    data = [[NSMutableData alloc] init];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (!connection) {
        // this is better if you @throw an exception here
        NSLog(@"error while starting the connection");
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [data setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)someData {
    [data appendData:someData];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Error %@", error);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSError *error;
    id jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
    data = nil;
    if(jsonData) {
        NSInteger versie = [[jsonData objectForKey:@"version"] intValue];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setInteger:versie forKey:@"version"];
        NSArray* jsonItems = [jsonData objectForKey:@"questions"];
        NSLog(@"%d updates, versie %d", [jsonItems count], versie);
        DatabaseManager *db = [DatabaseManager instance];
        for(NSDictionary *v in jsonItems) {
            Vraag *vraag = [[Vraag alloc] init];
            vraag.uid = [[v objectForKey:@"id"] intValue];
            vraag.vraag = [v objectForKey:@"vraag"];
            vraag.antwoord = [v objectForKey:@"antwoord"];
            vraag.categorie = [[v objectForKey:@"categorie"] intValue];
            [db updateVraag:vraag];
        }
    } else {
        NSLog(@"Error %@", error);
    }
    
}


@end
