//
//  ChooseModeController.m
//  quiz
//
//  Created by Jonas on 20/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import "ChooseModeController.h"
#import "AppDelegate.h"

@interface ChooseModeController ()

@end

@implementation ChooseModeController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Mode";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)selectNormal:(id)sender {
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    app.mode = NSClassFromString(@"NormalMode");
    [self performSegueWithIdentifier:@"segueCategorie" sender:self];
}

-(IBAction)selectSurvival:(id)sender {
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    app.mode = NSClassFromString(@"SurvivalMode");
    [self performSegueWithIdentifier:@"segueCategorie" sender:self];
}

-(IBAction)selectTimeAttack:(id)sender {
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    app.mode = NSClassFromString(@"TimeAttackMode");
    [self performSegueWithIdentifier:@"segueCategorie" sender:self];
}

@end
