//
//  ChooseCategoryController.m
//  quiz
//
//  Created by Jonas on 20/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import "ChooseCategoryController.h"
#import "DatabaseManager.h"
#import "AppDelegate.h"

@interface ChooseCategoryController ()

@end

@implementation ChooseCategoryController {
    NSArray *tableData;
}

@synthesize tableView;
@synthesize checkBool;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    tableData = [NSArray arrayWithObjects:@"Strips", @"Geografie", @"Kunst", @"Wetenschap", @"Algemeen", @"Film", @"Geschiedenis", @"Muziek", @"Sport", @"Taal", nil];
    
    self.title = @"Categorieën";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell2";
    
    UITableViewCell *cell = [tView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle
                reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell.
    UIImage *cellImage = [UIImage imageNamed:@"apple.png"];
    cell.imageView.image = cellImage;
    
    DatabaseManager *db = [DatabaseManager instance];

    
    cell.textLabel.text = [tableData objectAtIndex: [indexPath row]];
    NSInteger cat = [indexPath row] + 1;
    NSString *subtitle = [NSString stringWithFormat:@"Opgelost: %d/%d", [db getSolvedCount:cat], [db getTotalCount:cat]];
    cell.detailTextLabel.text = subtitle;
    return cell;
}

-(IBAction)selectAll:(id)sender {
    if (!checkBool){
    for (NSInteger s = 0; s < self.tableView.numberOfSections; s++) {
        for (NSInteger r = 0; r < [self.tableView numberOfRowsInSection:s]; r++) {
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:r inSection:s]
                                        animated:NO
                                  scrollPosition:UITableViewScrollPositionNone];
        }
    }
    }else{
        for (NSInteger s = 0; s < self.tableView.numberOfSections; s++) {
            for (NSInteger r = 0; r < [self.tableView numberOfRowsInSection:s]; r++) {
                [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:r inSection:s] animated:NO];
            }
        }
    }
    checkBool = !checkBool;
}

-(IBAction)start:(id)sender {
    if ([tableView indexPathForSelectedRow] == nil){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Geen categorie geselecteerd"
                                                        message:[NSString stringWithFormat:@"U moet minstens 1 categorie selecteren"]
                                                       delegate:self
                                              cancelButtonTitle:@"Oke"
                                              otherButtonTitles:nil];
        [alert show];
    }else{
    DatabaseManager *db = [DatabaseManager instance];
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    app.vragen = [db getVragen:[tableView indexPathsForSelectedRows]];
    NSLog(@"%@", app);
    [self performSegueWithIdentifier:@"segueStartGame" sender:self];
    }
}

@end
