//
//  ChooseCategoryController.h
//  quiz
//
//  Created by Jonas on 20/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseCategoryController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITableView *tableView;
}

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (assign) BOOL checkBool;
-(IBAction)selectAll:(id)sender;
-(IBAction)start:(id)sender;
@end
 