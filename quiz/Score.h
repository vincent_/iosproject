//
//  Score.h
//  quiz
//
//  Created by Jonas on 20/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import <Foundation/Foundation.h>
  
@interface Score : NSObject

@property NSString *naam;
@property NSInteger score;

@end
  