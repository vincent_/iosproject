//
//  SubmitScoreController.h
//  quiz
//
//  Created by Jonas on 20/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubmitScoreController : UIViewController {
    IBOutlet UILabel *scoreLabel;
    IBOutlet UITextField *nameTxt;
    NSMutableData *data;
}
@property NSInteger score;
@property NSString *mode;

@property UILabel *scoreLabel;
@property UITextField *nameTxt;

-(IBAction)submitScore:(id)sender;

@end
 