//
//  NormalMode.h
//  quiz
//
//  Created by Jonas on 20/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModeDelegate.h"

@interface NormalMode : NSObject<ModeDelegate>
 

@end
