//
//  GameController.m
//  quiz
//
//  Created by Jonas on 20/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import "GameController.h"
#import "AppDelegate.h"
#import "Vraag.h"
#import "DatabaseManager.h"
#import "SubmitScoreController.h"

@interface GameController ()

@end

@implementation GameController

@synthesize delegate;
@synthesize time;
@synthesize progress;
@synthesize score;
@synthesize vraagCount;
@synthesize antwoordTxt;
@synthesize vraagCountLabel;
@synthesize vraagLabel;
@synthesize hint;
@synthesize scoreLabel;

Vraag *vraag;
NSTimer *hintTimer;
NSMutableArray *hintSpots;
bool stop;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"Start: %d", stop);
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.delegate = [[app.mode alloc] init];
    self.title = [self.delegate getTitle:self];
	// Do any additional setup after loading the view.
    timer = [NSTimer scheduledTimerWithTimeInterval:0.25
                                     target:self
                                   selector:@selector(tick:)
                                   userInfo:nil
                                    repeats:YES];
    hintTimer = [NSTimer scheduledTimerWithTimeInterval:3
                                             target:self
                                           selector:@selector(giveHint:)
                                           userInfo:nil
                                            repeats:YES];
    [antwoordTxt addTarget:self action:@selector(handleEdit:) forControlEvents:UIControlEventAllEditingEvents];
    [antwoordTxt becomeFirstResponder];
    UISwipeGestureRecognizer *mSwipeLeftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:) ];
    
    [mSwipeLeftRecognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    
    [self.view addGestureRecognizer:mSwipeLeftRecognizer];
    stop = false;
    time = 100;
    hintSpots = [[NSMutableArray alloc] init];
    [self next];
   
}

- (void) handleEdit:(UIControlEvents)events {
    if([[self normalize:[antwoordTxt.text lowercaseString]] isEqualToString:[self normalize:vraag.antwoord]]) {
        DatabaseManager *db = [DatabaseManager instance];
        [db setDone:vraag];
        [delegate onCorrectAnswer:self];
        [self next];
    }
}
 
- (void) tick:(NSTimer *) t {
    time--;
    [progress setProgress:time/100.0];
    [delegate onTick:self];
    [progress setNeedsDisplay];
    if(time == 0) {
        [delegate onTimeout:self];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Tijd is op"
                                                        message:[NSString stringWithFormat:@"Het antwoord was: %@", vraag.antwoord ]
                                                       delegate:self
                                              cancelButtonTitle:@"Volgende"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void) giveHint:(NSTimer *) t {
    if([hintSpots count] > 1) {
        NSInteger rnd = arc4random() % [hintSpots count];
        NSInteger spot = [[hintSpots objectAtIndex:rnd] intValue];
        [hintSpots removeObjectAtIndex:rnd];
        NSMutableString *s = [[NSMutableString alloc] initWithString:hint.text];
        NSLog(@"%@ %d", s, spot);
        [s replaceCharactersInRange:NSMakeRange(spot * 2, 1) withString:[NSString stringWithFormat:@"%c", [vraag.antwoord characterAtIndex:spot]]];
        hint.text = s;
    }
    
}

- (void) next {
    if([delegate hasRemaining:self]) {
        vraagCount++;
        AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        vraag = [app.vragen lastObject];
        [app.vragen removeLastObject];
        vraagLabel.text = vraag.vraag;
        antwoordTxt.text = @"";
        NSLog(@"Antwoord: %@", vraag.antwoord);
        NSMutableString *hintStr = [[NSMutableString alloc] init];
        int len = [vraag.antwoord length];
        [hintSpots removeAllObjects];
        for(int i = 0; i < len; i++) {
            unichar c = [vraag.antwoord characterAtIndex:i];
            if(c == ' ' || c == ',' || c == '"' || c == '\'' || c == '?' || c == '!' || c == '.' || c == '-') {
                [hintStr appendFormat:@"%c ", c];
            } else {
                if(c != 'a' && c != 'u' && c != 'e' && c != 'o' && c != 'i' && c != 'A' && c != 'U' && c != 'E' && c != 'O' && c != 'I') {
                    //Klinkers worden niet als hint gegeven
                    [hintSpots addObject:[NSNumber numberWithInt:i]];
                }
                [hintStr appendFormat:@"_ "];
            }
        }
        hint.text = hintStr;
        
        [delegate onNextRound:self];
    } else {
        NSLog(@"Game Over");
        if(!stop) {
        stop = true;
        [self performSegueWithIdentifier:@"segueSubmitScore" sender:self];
        }
    }
}

- (void)onSwipe:(UIGestureRecognizer *)gesture {
    [delegate onSwipe:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated {
    [timer invalidate];
    [hintTimer invalidate];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    SubmitScoreController *dest = (SubmitScoreController *) [segue destinationViewController];
    dest.score = score;
    dest.mode = [delegate getTitle:self];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self next];
}


- (NSString *)normalize:(NSString *)string {
    NSError *error = NULL;
    string = [string lowercaseString];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[^a-z0-9]" options:NSRegularExpressionCaseInsensitive error:&error];
    NSString *modifiedString = [regex stringByReplacingMatchesInString:string options:0 range:NSMakeRange(0, [string length]) withTemplate:@""];
    return modifiedString;
}
@end
