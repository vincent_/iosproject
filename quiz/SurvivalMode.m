//
//  SurvivalMode.m
//  quiz
//
//  Created by Jonas on 20/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import "SurvivalMode.h"
#import "GameController.h"

@implementation SurvivalMode
@synthesize lives;

-(id)init {
    self.lives = 3;
    return self;
}

-(void)onCorrectAnswer:(GameController *)game {
    game.score += game.time;
    game.scoreLabel.text = [NSString stringWithFormat:@"Score: %d", game.score];
}

-(void)onNextRound:(GameController *)game {
    game.time = 100;
    game.vraagCountLabel.text = [NSString stringWithFormat:@"Levens: %d", lives];
}

-(void)onTimeout:(GameController *)game {
    lives--;
}

-(NSString *)getTitle:(GameController *)game {
    return @"Overleving";
}
-(bool)hasRemaining:(GameController *)game {
    return lives > 0;
}

- (void)onSwipe:(GameController *)game { }
- (void)onTick:(GameController *)game { }

@end
