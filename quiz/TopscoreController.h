//
//  TopscoreController.h
//  quiz
//
//  Created by Jonas on 20/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopscoreController : UITableViewController <UITableViewDelegate, UITableViewDataSource, NSURLConnectionDelegate, NSURLConnectionDataDelegate, UIGestureRecognizerDelegate> {
    
    IBOutlet UITableView *tableView;
    NSMutableArray *topscores;
    NSMutableData *data;
    bool reloading;
    
}
 
@end
