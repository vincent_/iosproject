//
//  GameController.h
//  quiz
//
//  Created by Jonas on 20/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModeDelegate.h"

@interface GameController : UIViewController<UIGestureRecognizerDelegate, UIAlertViewDelegate> {
    id <ModeDelegate> delegate;
    IBOutlet UILabel *vraagCountLabel;
    IBOutlet UILabel *scoreLabel;
    IBOutlet UIProgressView *progress;
    IBOutlet UILabel *vraagLabel;
    IBOutlet UITextField *antwoordTxt;
    IBOutlet UILabel *hint;
    NSTimer *timer;
}
 
@property (retain) id delegate;
@property UIProgressView *progress;
@property UITextField *antwoordTxt;
@property UILabel *vraagCountLabel;
@property UILabel *vraagLabel;
@property UILabel *hint;
@property UILabel *scoreLabel;
@property NSInteger score;
@property NSInteger time;
@property NSInteger vraagCount;

- (void) onSwipe:(UIGestureRecognizer *)gesture;
- (void) next;

@end
