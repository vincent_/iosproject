//
//  SubmitScoreController.m
//  quiz
//
//  Created by Jonas on 20/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import "SubmitScoreController.h"

@interface SubmitScoreController ()

@end

@implementation SubmitScoreController

@synthesize scoreLabel;
@synthesize nameTxt;
@synthesize score;
@synthesize mode;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Score opslaan";
    scoreLabel.text = [NSString stringWithFormat:@"Score: %d", score];	// Do any additional setup after loading the view.
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    nameTxt.text = [prefs stringForKey:@"name"];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [data setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)someData {
    [data appendData:someData];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Error %@", error);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)con {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSInteger userid = [prefs integerForKey:@"userid"];
    NSString *name = [prefs stringForKey:@"name"];
    if(userid == 0 || ![name isEqualToString:nameTxt.text]) {
        userid = [[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] intValue];
        [prefs setInteger:userid forKey:@"userid"];
        [prefs setObject:nameTxt.text forKey:@"name"];
    }
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://spacedicks.herokuapp.com/highscores/?mode=%@&userid=%d&score=%d",mode, userid, score]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:nil];
    if (!connection) {
        // this is better if you @throw an exception here
        NSLog(@"error while starting the connection");
    }
    [self.navigationController popToRootViewControllerAnimated:NO];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
 
- (void)submitScore:(id)sender {
    if(nameTxt.text == @"") {
        return;
    }
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSInteger userid = [prefs integerForKey:@"userid"];
    NSString *name = [prefs stringForKey:@"name"];
    if(userid == 0 || ![name isEqualToString:nameTxt.text]) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://spacedicks.herokuapp.com/createuser/?naam=%@",[nameTxt.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
        data = [[NSMutableData alloc] init];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (!connection) {
            // this is better if you @throw an exception here
            NSLog(@"error while starting the connection");
        }
    } else {
        [self connectionDidFinishLoading:nil];
    }
    


}

- (void) viewWillAppear:(BOOL)animated {
    //Verberg navigatie
    self.navigationItem.hidesBackButton = YES;
}

@end
