//
//  NormalMode.m
//  quiz
//
//  Created by Jonas on 20/11/12.
//  Copyright (c) 2012 Programitors. All rights reserved.
//

#import "NormalMode.h"
#import "GameController.h"

@implementation NormalMode
 
- (void)onCorrectAnswer:(GameController *)game {
    game.score += game.time;
    game.scoreLabel.text = [NSString stringWithFormat:@"Score: %d", game.score];
}

- (void)onNextRound:(GameController *)game {
    game.time = 100;
    game.vraagCountLabel.text = [NSString stringWithFormat:@"Vraag: %d/%d", game.vraagCount, 20];
}

- (NSString *)getTitle:(GameController *)game {
    return @"Normaal";
}
- (bool)hasRemaining:(GameController *)game {
    return game.vraagCount < 20;
}

- (void)onTimeout:(GameController *)game { }
- (void)onSwipe:(GameController *)game { }
- (void)onTick:(GameController *)game { }

@end
